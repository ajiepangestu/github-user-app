package com.dicoding.githubuserapp

data class UserDataList(val users: List<UserData>)

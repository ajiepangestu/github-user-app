package com.dicoding.githubuserapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.dicoding.githubuserapp.Utils.getJsonFromAssets
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_user_list.*


class UserListActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_list)

        val mJsonFileString = getJsonFromAssets(this, "GithubUser.json")
        val mUserDataList = Gson().fromJson(mJsonFileString, UserDataList::class.java)

        mRcUser.adapter = UserAdapter(this, mUserDataList)
    }
}

package com.dicoding.githubuserapp

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.card.MaterialCardView
import kotlinx.android.synthetic.main.user_row.view.*


class UserAdapter(private val mContext: Context, private val mUserDataList: UserDataList) :
    RecyclerView.Adapter<UserAdapter.UserViewHolder>() {
    override fun onCreateViewHolder(mParent: ViewGroup, mViewType: Int): UserViewHolder {
        return UserViewHolder(
            LayoutInflater.from(mParent.context).inflate(R.layout.user_row, mParent, false)
        )
    }

    override fun getItemCount(): Int {
        return mUserDataList.users.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(mUserViewHolder: UserViewHolder, mPosition: Int) {
        mUserViewHolder.mTvName.text = mUserDataList.users[mPosition].name
        mUserViewHolder.mTvLocation.text = mUserDataList.users[mPosition].location
        mUserViewHolder.mTvFollower.text = "${mUserDataList.users[mPosition].follower} Follower"
        mUserViewHolder.mTvFollowing.text =
            " | ${mUserDataList.users[mPosition].following} Following"
        val mImageResource: Int = mContext.resources.getIdentifier(
            "@drawable/user${mPosition + 1}",
            null,
            mContext.packageName
        )
        ContextCompat.getDrawable(mContext, mImageResource)?.let {
            mUserViewHolder.mIvAvatar.setImageDrawable(it)
        }
        mUserViewHolder.mCvUser.setOnClickListener {
            val mIntent = Intent(mContext, UserDetailActivity::class.java)
            mIntent.putExtra("user", mUserDataList.users[mPosition])
            mIntent.putExtra("position", mPosition)
            mContext.startActivity(mIntent)
        }
    }

    class UserViewHolder(mItemView: View) : RecyclerView.ViewHolder(mItemView) {
        val mTvName: TextView = mItemView.mTvName
        val mTvLocation: TextView = mItemView.mTvLocation
        val mTvFollower: TextView = mItemView.mTvFollower
        val mTvFollowing: TextView = mItemView.mTvFollowing
        val mIvAvatar: ImageView = mItemView.mIvAvatar
        val mCvUser: MaterialCardView = mItemView.mCvUser
    }

}

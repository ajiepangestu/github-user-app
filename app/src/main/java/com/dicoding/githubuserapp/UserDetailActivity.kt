package com.dicoding.githubuserapp

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_user_detail.*

class UserDetailActivity : AppCompatActivity() {
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_detail)

        val mUser = intent.getParcelableExtra<UserData>("user")
        mUser?.let {
            val mImageResource: Int = this.resources.getIdentifier(
                "@drawable/user${intent.getIntExtra("position", 0) + 1}",
                null,
                this.packageName
            )
            ContextCompat.getDrawable(this, mImageResource)?.let { mDrawable ->
                mIvAvatar.setImageDrawable(mDrawable)
            }
            mTvUsername.text = it.username
            mTvName.text = it.name
            mTvCompany.text = it.company
            mTvLocation.text = it.location
            mTvRepository.text =
                if (it.repository > 1) "${it.repository} Repositories" else "${it.repository} Repository"
            mTvFollowing.text =
                if (it.following > 1) "${it.following} Followings" else "${it.following} Following"
            mTvFollower.text =
                if (it.follower > 1) "${it.follower} Followers" else "${it.follower} Follower"
        } ?: run {
            this.finish()
        }
    }
}
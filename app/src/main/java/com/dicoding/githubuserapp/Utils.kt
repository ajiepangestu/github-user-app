package com.dicoding.githubuserapp

import android.content.Context
import java.io.IOException
import java.io.InputStream
import java.nio.charset.Charset

object Utils {
    fun getJsonFromAssets(mContext: Context, mFileName: String): String? {
        val mJsonString: String
        mJsonString = try {
            val mInputStream: InputStream = mContext.assets.open(mFileName)
            val mSize: Int = mInputStream.available()
            val mBuffer = ByteArray(mSize)
            mInputStream.read(mBuffer)
            mInputStream.close()
            String(mBuffer, Charset.forName("UTF-8"))
        } catch (e: IOException) {
            e.printStackTrace()
            return null
        }
        return mJsonString
    }
}
